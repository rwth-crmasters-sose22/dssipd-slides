---
marp: true
---

# DSSIPD-SoSe22 - Class 04
## Git

---

<style>
img[alt~="center"], .center {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
li {
    clear: right;
}
.padding-top {
    margin-top: 1em;
}
</style>

# Before we begin

- [Course concept/pace informal evaluation](https://www.menti.com/51g5a1rmet)
- _Match-making/grouping requirement_ for the DDP module:
    - Every team member must be from a different discipline and from a discipline cultural background (i.e. a different mother tongue)
- We have ==1 incorrect submission==, ==2 non-conforming filenames==, and a ==few missing submissions== for the DSSIPD assignment :/

---

# Git

![center h:100px](./assets/git-logo.svg)
Git is the _type of directory_ that we use to store, share, and collaborate on all forms of software. Software is not only executable code, it is code that renders in a website (e.g. HTML, markdown) or code that renders to images (e.g. SVG). All of it is suitable for git.

---

# GitLab and GitHub

It is this "git", that we see in the first part of the name of these two websites:

<span>![h:140px](./assets/github-logo.svg) ![h:170px](./assets/gitlab-logo.svg)</span>{.center}

This is because, these two websites distribute software in the form of _git directories_, which we are more appropriately called ==_git repositories_==.

---

# Who needs to use git

Anyone who is:
- ... writing or modifying code
- ... handing over or managing code, without understanding of how it works
- ... looking for information in code, finding bugs and knowing when they occured
- ... analysing contributions (e.g. to determine legal ownership of the code)
- ... using and running the code for personal and commercial purposes

---

# What do you need to use git

- The git _tooling_ must be installed on your computer (let's check the version)
- Optionally: an editor with a _git integration_ (like **vscode**)

---

# Git tooling version

In both Mac OS X (bash) and Windows (PowerShell):
```
git --version
```

Try running this command in your local environment. This would be, PowerShell if you are using Windows, or the Terminal if you are using Mac OS X.

---

# We will be using the Shell, a lot!

It helps to remember the anatomy of a modern command from last class:

# Command Basics

```bash
command subcommand -a -b -c --abc --cde target1 target2
```

**Remember:**
- Not all commands will have all of these parts
- Simple `command`s do not need a `subcommand`, for example (e.g. most of the bash commands like `whoami`, `pwd`, `cd`, `ls`, etc.)
- `target1` and `target2` can be many things, like folders, files, URL's, etc.

---

# Your home directory

In Mac OS X (bash):
```
finder ~
```

In Windows (PowerShell):
```
explorer ~
```

> Once executed, this command should open up your home directory in the system's file explorer (![h:1em](./assets/windows-explorer-icon.svg) on Windows,![h:1em](./assets/macosx-finder-icon.png) on Mac OS X){.padding-top}

> For more information **Google**: _"tilde dot double dot linux"_

---

# Create a repos directory

In Mac OS X (bash):
```
mkdir ~/repos
```

In Windows (PowerShell):
```
mkdir ~\repos
```

> Notice that in addition to the differences mentioned in [this slide](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.en.html#30), Windows uses the _backward slash_ <kbd>/</kbd> as the path separator, whereas Mac OS X & Linux use the _forward slash_ <kbd>\\</kbd>. Keep this in mind when switching between Windows and other systems 💡{.padding-top}

---

# Git Assignment (deadline 03.05 midnight)

_As agreed in class. We will slow down a little bit and take the rest of our Git learning to the session on 04.05.2022_

The goal from this adjustment is to **give you some more time to master the shell to the level of comfort described in [this slide](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.en.html#21),** to ensure that you do not get overwhelmed or face major difficulties when trying to master the basics of git.

==‼️ Please use this opportunity wisely ‼️==

---

# Assignment directions

Using the GitLab interface and the knowledge from previous classes (i.e. markdown, and the shell history), follow the instructions in the `git-class` gitlab repo: https://gitlab.com/rwth-crmasters-sose22/course-work/git-class

Namely, you will have to browse to the `content` folder in the repository, then to the `people`'s subfolder. From there, follow the contribution guidelines in the README file

---

# Before next class

- Master the shell to the level of comfort required
- Re-upload the shell-history assignment to reflect the above
- Do the assignment above
- Read the slides for the next class and start practicing (preferably by setting up the SSH certificates ahead of time)
