---
marp: true
---

# DSSIPD-SoSe22 - Class 05
## Git Workflow

---

<style>
img[alt~="center"], .center {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.clear-none {
    width: 500px;
    float: left;
}
li {
    clear: none ;
}
.padding-top {
    margin-top: 1em;
}
.padding-top2 {
    padding-top: 1em;
}
</style>

# Before we begin

- _LAN-based Screen sharing_ at [https://192.168.0.178:8443/](https://192.168.0.178:8443/) and type "test"
- Revise assignments from last lecture
- Coffee bot in Discord is active! Please check the pinned message in the **#off-topic** channel on the Discord server.
- Next DDP session is **the first _industry feedback_ session,** with a guest lecturer from _Leonhard Weiss GmbH_
- DDP [markdown slide template](https://gitlab.com/rwth-crmasters-sose22/templates/slide-template) on GitLab in [templates](https://gitlab.com/rwth-crmasters-sose22/templates) subgroup
- DSSIPD slide issues & wiki
- Proposed challenges & mini challenges
---

# Git workflow roadmap

Our plan with regard to git, is teaching you how to use the `git` command to do the following basic tasks:
- [ ] _==&leftarrow; configure git and SSH==_{.float-right}Enter your information in git, create an SSH certificate
- [ ] _==&leftarrow; clone repo using SSH==_{.float-right}Download a remote repository to your computer
- [ ] _==&leftarrow; create a branch==_{.float-right}Create your own working copy of the repository
- [ ] _==&leftarrow; commit changes to your branch==_{.float-right}Make changes to your working copy
- [ ] _==&leftarrow; merge from **main**/**master** periodically==_{.float-right}Keep your work up to date with the remote
- [ ] _==&leftarrow; push branch==_{.float-right}Upload the changes you made and create a merge request in GitLab

---

# Find the URL of the repository to download/clone it

Now, we are ready to download, or more appropriately called, `clone` the [repository at for the git class](https://gitlab.com/rwth-crmasters-sose22/course-work/git-class).

This is the repository called `git-class` inside the `course-work` GitLab group, which resies inside of the course's `rwth-crmasters-sose22` GitLab group. 

The git subcommand needed to do this is `clone`. The main command is `git`. This implies that `git clone` will be used. The _target_ is the clone URL.

---

# Clone URL

![center h:400px](./assets/gitlab-clone-URL.png)

---

# What is a clone URL

The clone URL is the path we need to pass the `git clone` command as the `target`. It is similar to the identifier we use to refer to a Docker Hub container in the case of the `docker run` command.

In order to clone a repository from either GitHub or GitLab, one needs a clone URL, as simple as that.

This URL is different from what we use to view the repository in the browser, namely, it ends with `.git`

---

# SSH or HTTPS

<div><img width=500 src="./assets/gitlab-clone-URL-choice.png" class="float-right" />To clone a repo using <code>git clone</code>, one can clone using either HTTPS or SSH.  

<p></p>


- The SSH clone URL starts with `git@`
- The HTTPS clone URL starts with `https://`

</div>

---

# Which one to choose

The difference between the two cloning methods is in ==how you prove to the server (i.e. GitLab or GitHub) that you are who you are claiming to be.== That is, the difference lies in **the authentication process.**

If you want to use the logins (e.g. the @gitlab username/password you use to login in the browser) to authenticate using the `git` commands, **then use the HTTPS method**.

If, on the other hand, you are dealing with an organizational GitLab instance (e.g. the RWTH GitLab instance), then you cannot use your SSO (i.e. @rwth-sso) credentials, for both safety and practical reasons. In this case, we **have to use the SSH method**.

---

# Setting up the SSH method

In this class, we show you how to use the SSH method, which is a great option if you are on a computer or a machine that you are going to use repeatedly to access repositories.

Instead of using a username and a password, SSH uses an automatically-generated certificate. ==It is like a long, and a very secure password.==

> What is interesting about certificates is that you have two of them, one secret, which stays on the computing device you are setting up (called the private certificate), and one that is distributed to others who want to authenticate you, which you authorize in your account on GitLab (called the public certificate).

---

# Sounds complicated, but is it

Run the following commands in your local environment:

In Mac OS X (bash):
```
ls ~/.ssh
```

In Windows (PowerShell):
```
ls ~\.ssh 
```

If you have ever used SSH on your device before, then you must see a list of files in the `.ssh` hidden directory inside of your home directory.

> For more info, please Google "dot files and hidden directories"

---

# Create your first certificate

Run the following commands in your local environment:

In both Mac OS X (bash) and Windows (PowerShell):
```
ssh-keygen -t ed25519
```


**Accept all the defaults for the command (Press <kbd>ENTER</kbd> 3 times)**

> If in doubt on how this means, please use the command `ssh-keygen --help` and look out for the `t` option. Alternatively, use the shell-class container to seek the simplified man page. This will show up in your shell-history experimentation file.


---

# What just happened?

Let's now try the same commands as before:

In Mac OS X (bash):
```
ls ~/.ssh
```

In Windows (PowerShell):
```
ls ~\.ssh 
```

Unlike before, you must see that two files had been added to this hidden directory, one without an extension (i.e. the private certificate) and one with a `.pub` at the end (i.e. the public certificate).

---

# Complete the setup

![h:400 center](./assets/gitlab-ssh-keys.png)

Now we must copy and paste the public key in our GitLab account, and we are done! (try to follow the instructions, if you need help, search online 😉)

--- 

# Test SSH authentication

Even though we won't be using the `ssh` command directly, it is indirectly used by the `git` command when cloning and pushing using an SSH URL for the repository.

Hence, and to test if the certificate is working, use the following command on Mac OS X (Bash) and Windows (PowerShell):
```
ssh git@gitlab.com
```

If this is the first time a connection is made from your device to GitLab using SSH, then a warning will appear. Type "yes" and press <kbd>ENTER</kbd>.
```
The authenticity of host 'git.rwth-aachen.de can't be established.
ED25519 key fingerprint is SHA256:***.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

---

# Clone the git-class repository using SSH

Command to run **from the repos folder** on Mac OS X (Bash) and Windows (PowerShell):
```
git clone git@gitlab.com:rwth-crmasters-sose22/course-work/git-class.git
```

> Self-assessment questions:
> - what is the main command above, what is the subcommand
> - what is the target, in the command
> - where was this target obtained from?
> - would this command work if we did not do the SSH setup?

---

# Local vs. remote repository

The folder we created through this process of cloning is called a _local repository_, to distinguish it from the one on GitLab/GitHub, which is called, the _remote repository_.

The local repository folder contains the files currently in the repository. These will be in the working directory.

But it will also have the full revision database in a hidden subfolder called `.git`. If this subfolder is ever deleted, then all ==the git-related information will be lost, and the repository folder will turn from a repository to just a regular folder.==

> _Self-learning question_: which git command turns a regular folder into a repository?

> _Challenge question_: how to reset a git repository in the shell? (hint: you have to delete the git history, then initialize a new repository using the files in the folder)

---

# The local repository

A local repository on your device will contain:
- information about the remote repositories to which the local repository is connected (check them with `git remote -v`)
- branches in the local repository, which may or may not track branches on the remote repository (check them with `git branch -v`)

> Note that the `-v` option is a shortcut for `--verbose` which is common to many shell commands. It gives you more elaborate information about something.{.padding-top}

---

<style>
    .starship-prompt {
        font-family: "FiraCode Nerd Font";
        background-color: none;
        text-decoration: none;
        color: red;
    }
</style>

# Navigate to the repo

If the clone was successful, then you will have a folder named `git-class` which will be created inside of the repos folder, with the repository in it.

> _Self-assessment_: How do you navigate to the repos folder from the Terminal? Answer for both Mac OS X and Windows ❓

Using Starship 🚀, this is the view of the prompt:
==&leftarrow; Notice the "master" part, it only appears when in the directory=={.float-right}![](./assets/starship-master-branch.png)

---

# git-configure commands

Each time you record a change in the repo, it is signed with a _name_ and an _email_. This is the way git signs recorded changes, also called, **git commits**.

Please find below the commands needed on Windows and Mac OS X to tell git how to sign your commits:
```
git config --global user.name "First Last"
git config --global user.email "first.last@rwth-aachen.de"
```

> Note that the information above is just for helping us know who made the changes in the repo. **These are not the logins!**

Even though you can sign your commits as _Kn$ght439_ at _catchme@phony.null_, please do not do that! Use your full name and your RWTH email :)

---

# git-remote commands

The word _remote_ in git is a shortcut for a _remote repository_, and a one that is likely linked to the local repository.

This is how the `git pull` (i.e. download) and `git push` (i.e. upload) commands know which source and destination use for syncing the local repository.

To view all the remotes linked to the local repository use this command:
```
git remote -v
```

> Note that a _remote_, beside its URL, is also given a name. Historically, the name "origin" had been used to the refer to the remote repository a local repository is linked with, and is still used in the majority of git repositories today.

---
# git-branch commands

Branching is when you create working copies of the repo in parallel, to list all the local branches type the following command in the shell:
```
git branch -v
```

Create a new branch `new-branch`:
```
git branch new-branch
```

Switch to `new-branch`:
```
git checkout new-branch
```

Starship 🚀 prompt:
==&leftarrow; Notice the "other-branch" part=={.float-right}![](./assets/starship-other-branch.png)

---

# git-commit commands

Committing is when you record the changes you made in the repository.

Navigate to the root of the repository and type the following commands in the shell:
```
git add .
git commit -m "message"
```

If there are no new files added, then:
```
git commit -am "message"
```

> _Commit messages_: can be made of multiple lines, the first line is recommended to be a summary with 50 characters, followed by two new-line characters and a more detailed explanation if needed.{.padding-top}
---

# Merging in git

Merging is an important concept in git.

Merging (using the `git merge` command) combines changes from another branch, into the current branch.

_Merging can be used to:_
- incorporate changes from the **main**/**master** branch to your branch, or  
    > can you explain who and why would someone need to do that?
- to merge one of the branches into the **main**/**master** branch  
    > can you do that? who and when is this operation needed?

---

# git-merge commands

Merging in git is easy, the _target_ of the command is the branch you need to merge into yours. This could be the local **main**/**master** branch or the one on the remote (usually called _origin_). In which case we use **origin/main** or **origin/master**.

Hence the commands needed to periodically keep your local branch up to date with the repository are the following (assuming a main branch called **master**):
```
git fetch
git merge origin/master
```

> _Self-study_: use Google, the search engine, to research what the `git fetch` command does. Use the simplified **tldr; man pages** for this.

---

# There will be conflicts!

Occasionally, you will run into situation where merging from the **main**/**master** on the _origin_ will lead to the same file that you edited being updated while you are away making changes on your computer.

==This leads to a situation known as a _conflict_.==

The situation is resolved by going over the content of the conflicting file and resolving each one of the _conflict sections_ that are present in it. This is particularly easy in Vscode and all you need is to choose, for each conflict, whether you want to keep your change (i.e. in green) or the change from the origin (i.e. in red).

==Only after resolving all the conflicting sections in the file, that you should stage it and commit it before continuing to work on your branch.==

---
# git-push commands

Pushing is when you upload the changes you made either in the **main** branch or any other branch, to the remote repository (on GitLab or GitHub).

The command for this is simple:
```
git push
```

Note that if this is the first time you are pushing from your local branch, then the command becomes:
```
git push -u origin <your-local-branch>
```

After pushing your branch, it is time to create a request that the branch you made be incorporated (or **merged**) into the **main** branch.

---

# Create a merge request in GitLab

A _merge request_ is not a git CLI concept, rather, it is a GitLab concept.

In GitHub these are called _pull requests_, which are basically the same thing as a _merge request_ on GitLab.

A merge request tells the maintainer of the website to incorporate your changes into the **main**/**master**. In the GitLab interface, you can attach explanations, images, files and other types of media into these merge requests to convince the maintainer that your changes are good to go (for example, that they won't break the pipeline x-))

Please check the [documentation here](https://gitlab.com/rwth-crmasters-sose22/course-work/git-class) in the git-class repo for screenshots.

---

# Am I really expected to do all that in the shell?

Admittedly, we rely on Vscode's ability to perform most of these tasks from the GUI (i.e. the graphical user interface).

However, we expect you to try some of the shell commands in parallel, as you start warming up to _git_ using Vscode's excellent _git integration._

The idea is that even though in most cases you would use Vscode's GUI to perform git operations, ==there will be instances where it will be easier to do things from the shell.==

Furthermore, knowing the underlying shell commands  _will always make it easier_ for you to know what to look for when starting to use a new git graphical client.

---


# Git Resources (1)

- [Git the Simple Guide](https://rogerdudler.github.io/git-guide/). Great simplicity.
- [Learning Git One Commit at a Time](https://gitready.com/). What is excellent about this guide is that it clearly indicates how to start and what is basic vs. advanced.
- [Git Explorer](https://gitexplorer.com/) is a website for looking up git commands.
- [Learn Git Branching](https://learngitbranching.js.org/) is a game in the browser that is focused on teaching git from a visual-tree standpoint.

---
# Git Resources (2)

- [Visual Git Reference](https://marklodato.github.io/visual-git-guide/index-en.html).
- [GitHub Cheat Sheet](https://training.github.com/). Translation in most languages.
- [Visual Git Cheat Sheet](https://ndpsoftware.com/git-cheatsheet.html#loc=index;). A little advanced, but interesting.
- [When things Go Wrong](http://justinhileman.info/article/git-pretty/git-pretty.png), always makes me laugh 8-)
- [Git Immersion](https://gitimmersion.com/) is a practical course into git by deceased [Jim Weirich](https://en.wikipedia.org/wiki/Jim_Weirich).

---
# Git Resources (3)

- [GitHug](https://hub.docker.com/r/orwa84/githug) is a game that teaches you git in the terminal.
- [Git Tower](https://www.git-tower.com/), a graphical client for Windows and Mac OS X with extensive educational content in [video](https://www.git-tower.com/help/videos/learning-git-with-tower/), [guides](https://www.git-tower.com/help/guides/) and [a book](https://www.git-tower.com/learn/git/ebook). Students get the full version for free [here](https://www.git-tower.com/students/).
- [YouTube Series](https://www.youtube.com/playlist?list=PLEACDDE80A79CE8E7). Slightly older, maybe not very easy to follow.
