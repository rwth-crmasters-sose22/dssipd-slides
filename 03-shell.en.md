---
marp: true
---

# DSSIPD-SoSe22 - Class 03
## The Shell

---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
</style>

# Operating Systems, Seed Analogy  

![h:500px center](./assets/seed-analogy.drawio.png)

<!--

This is a confusing comment

-->
---

# Bash and PowerShell

In this class, we try to get to a level of comfort with these two shells:
- Bash
- PowerShell

It is probable that an innovator will most often deal and interact with the Bash shell, but sometimes, need to use PowerShell on Windows.

Hence it must be noted that _it is way more important_ to be comfortable with the Bash shell, than with PowerShell. However, one must not be unable to find their way around in PowerShell as this would impair their ability to innovate.

---

# Learning Strategy

> Unlike common belief, learning similar technologies in parallel reinforces the common concepts while highlighting the differences more clearly from an earlier stage.

> This will make confusion less likely to happen when the learner is forced to switch back and forth between similar technologies.

> One way to explain this, is that when you learn similar things in parallel, you create separate mental pathways from the start, for each one of the things you are learning, rather than accidentally re-using and altering the learnt pathways of the _similar_ knowledge, which will only lead to an ever lasting confusion between the two.

---

# Learning in Parallel

> ### NON-TECHNICAL EXAMPLE OF WHAT WE MEAN BY THAT

> You now live in Germany, which is adjacent to a neighboring country with a similar language, the Netherlands.

> Even though not relevant to this course, it is worth noting that despite similarities in the language, the two countries are different enough, in terms of culture and workplace norms, to require a different social calibration.

> The language, on the other hand, is fairly similar. Similar enough to allow the learner to draw lots of meaningful parallels, but also similar that confusion can occur if differences and similarities are not established early on during learning.

> **In other words, we want to enjoy speaking German and Dutch simultaneously, rather than having to _painfully switch_ between the two all the time.**

---

# Before we Begin

Live trial of the **Coffee Bot** @Coffee on Discord at 13:45

> This bot costs 10 Euros a month. We would like to make a decision if we would like to fund it for 3 months to help us socialize.

- Sharing of screen over LAN for people at the back of the class: [https://192.168.0.178:8443/](https://192.168.0.178:8443/)
- Please use the meeting name "test" for the screen sharing above.
---

# The docker command

On all computers, we must have the following:
- A _console application_ (on our laptops: **Windows Terminal**, found by searching `wt`)
- A _Nerd font_ (**FiraCode NF Retina** recommended)
- Setting up your console to use the Nerd font.
- The `docker` command

Using the `docker` command, we are able to launch different pre-customized shells, environments, and applications from the command line 🤖

---

# The docker container for this class

Pre-customized shells, environments and applications that can be ran using the `docker` command are pulled from the [Docker Hub](https://hub.docker.com) website.

The word "<mark>Hub</mark>" in _Docker <mark>Hub</mark>_, is similar to the "Hub" part in _Git<mark>Hub</mark>_.

These are all **registries that serve software components**. In the case of GitHub and GitLab, we call these _repositories_. In the case of Docker hub, we call these _containers_.

The container we will be using for this class is the `orwa84/shell-class` container, which is hosted on Docker Hub at [https://hub.docker.com/r/orwa84/shell-class](https://hub.docker.com/r/orwa84/shell-class).

---

# Command Basics

```bash
command subcommand -a -b -c --abc --cde target
```

**Explanation:**
- `command`: this is the main command (e.g. "docker", "git", "apt", "npm", etc.)
- `subcommand`: this is one of many functions offered by the `command` (examples are "run", "clone", "update", "install", etc.)
- `a`, `b` and `c`: are one-character options, or modifiers, to tell the `subcommand` what to do, or how to do it.
- `abc` and `cde`: are multiple-character options, or modifiers, to tell the `subcommand` what to do, or how to do it.
- `target`: is the object being operated on, like a filename, reference, URL, etc.

---

# Docker command to run container for the class

Open your console application and type the following:

```bash
docker run -i -t --rm orwa84/shell-class
```

**Explanation:**
- `docker`: this is the main command
- `run`: this is the subcommand, it [runs a new container](https://tldr.ostera.io/docker-run)
- `-i` and `-t`: these options are used together (can also be abbreviated as `-it`). They ask Docker to run the containerized application and give you interactive access to it rather than run it in the background.
- `--rm`: tells docker to remove the container after it is exited (for testing purposes).
- `orwa84/shell-class`: is the container reference on [Docker Hub](https://hub.docker.com/r/orwa84/shell-class).


---

## Problem with the Previous docker command

Switch to the **Docker Desktop window** to see the running container you just now created.

Note that the running container will be given a random name (of the likes of "nervous_heyrovsky", "affectionate_haibt", and other strange, but humorous titles).

_In the interactive container prompt, type the command `exit`, press <kbd>ENTER</kbd>, and watch the container created disappear._

This is due to using the `--rm` option in the previous slide, which after exiting the container, removes it entirely with all of the data inside of it.

---

# Alternative Docker command

Now, open your console application and type the following:

```bash
docker run -i -t --name=class03 orwa84/shell-class
```

**Additional Explanation:**
- `--name`: this is an option to give the running container a name of your choosing.
- `=class03`: the `--name` option, unlike the `--rm` option, needs a value. This is the name you want to give to the container after running it. Values are assigned to options either using an equal sign `option=value` or a white space `option value` (in this case we used an equal sign).


---

# Inside the container

Once the above command is executed. You _are no longer typing commands on your local environment_. Rather, you are **now working inside of the containerized Linux environment.**

The container I made for this class is based on Ubuntu Linux, a type of Linux. It runs the Bash shell, and has the following programming languages installed out-of-the-box:
- Ruby ![h:1em](./assets/ruby-icon-logo.svg) (using the `ruby`, `irb` and `erb` commands).
- Python ![h:1em](./assets/python-icon-logo.svg) (using the `python`, `pip` and  command)
- Javascript ![h:1em](./assets/nodejs-icon-logo.svg) (using the `node`, `npm` and `yarn` commands)

We will discuss all of these commands later in the course, so do not worry.

---
# Class activity
## Getting the versions of all the commands

Modern commands are also called _tooling_.

Modern _tooling_ usually supports being called with a single, multi-character option: `version` and no target.

Using the general structure explained in slide 9 of modern commands, try to call each one of the commands in the previous slide with the `version` multi-character option to detect the versions of all the tooling commands offered inside the running container.

---

# Shell scopes

To exit the container or any other scope, we have a choice between:
- Typing the command `exit` and pressing <kbd>ENTER</kbd>
- Using the keyboard shortcut <kbd>CTRL</kbd> + <kbd>C</kbd> to exit while ending the process
- Using the keyboard shortcut <kbd>CTRL</kbd> + <kbd>D</kbd> to exit without ending the process


This applies to both exiting from the container, as well as existing from any nested scope _inside_ the container (for example, if you use the `irb` command to launch the interactive Ruby prompt inside the container)

---

# Shell scope illustration

> **[Local PowerShell prompt here]**
> _docker command to run or start **orwa84/shell-class**_
> > **[Bash shell inside the container]**
> > _`irb` command to launch the interactive Ruby_
> > > **[Ruby shell inside the bash shell inside the container]**
> > > <kbd>CTRL</kbd> + <kbd>C</kbd>
> >
> > Back to the bash shell inside the container
> > _type `exit` and press <kbd>ENTER</kbd>_
> >
> Back to the local PowerShell prompt 
> _type `exit` and press <kbd>ENTER</kbd>_

**The PowerShell windows closes...**

---
# Starting the container after exiting

So you exited the container and now want to start it again, but you use the command in **slide 12**, then you are likely to run into the following error:
```
docker: Error response from daemon: Conflict. The container name "class-03" is already in use by container.
You have to remove (or rename) that container to be able to reuse that name. 
```

### Reason for the error

The `docker` main command has many subcommands, as mentioned.

The `run` subcommand, is only capable of creating and [starting a new container](https://tldr.ostera.io/docker-run).

To start an existing container, one must use the `start` subcommand instead, which [starts an existing container that was either exited or stopped](https://tldr.ostera.io/docker-start).

---

# Restarting the container, the right command

```bash
docker start -i -a class03
```

**Explanation:**
- `docker`: this is the main command
- `start`: this is the subcommand, it [starts an existing container that had either been stopped or exited](https://tldr.ostera.io/docker-start)
- `-i` and `-a`: these options are used together (can also be abbreviated as `-ia`). They ask Docker to start the containerized application and give you interactive access to it rather than start it in the background.
- `class-03`: is the name given to the stopped or the exited container (from the `docker run` command in **slide 12**).

---

# Grading method

> Inside the container, everything you type is recorded to assist in the grading. This not a container behavior, just how [we designed this educational container to work](https://gitlab.com/rwth-crmasters-sose22/course-material/shell-class).

The grading process is based on the length of experimentation attempted by you inside the container. In fact, you will be graded by a computer script that compares your experimentation to everyone else in the class (called [relative grading](https://www.teachmint.com/glossary/r/relative-grading/)).

The experimentation follows the _self-learning method_, where you have the ability to read about certain commands, see examples, then use the information (along with your observations) to gain insight into how these commands (and the shell overall) work.

**This insight will then be heavily utilized throughout the DSSIPD and the DDP modules, which will be sufficient to _internalize and freeze the knowledge_.**

---

# The basics

The most useful command you need to know about is `man`.

This is the command you use to _get an explanation of all the other commands_. It stands for the "**man**ual pages". In the container, we use [simplified man pages](https://tldr.ostera.io/) called **tldr pages**.

Using this command, please look up and experiment with all of these commands in the container:

| `whoami` | `pwd` | `cd` | `ls` | `touch` | `mv` | `rm` | `cp` | `mkdir` | `cat` |
| -------- | ---- | ---- | ------- | ---- | ------- | ------- | ------- | ------- | ------- |
> These are essential commands, everyone, regardless of their background should be comfortable using them. There are numerous tutorials online. Please google: "essential bash commands" and check the links at the end of the presentation.

---
# How comfortable do you need to be

The level of comfort required with the shell is the following:
1. Know where you are in the filesystem, discover what else is there, and change the working directory to where you want to go (called: _navigation_)
2. Have a good understanding of what files are hidden, and what special directories like `.`, `..` and `~` mean.
3. Create, move, copy and remove single files.
4. Create, move, and copy empty folders.
5. Move and copy folders with everything inside of them (i.e. recursively).
6. Show the contents of one or more files in the shell.

---

# What else?

In addition to your experimentation with the bash shell itself, we would like to teach you _how to use the 3 programming languages_ inside the `orwa84/shell-class` container:
1. Ruby ![h:1em](./assets/ruby-icon-logo.svg)
1. Python ![h:1em](./assets/python-icon-logo.svg)
1. Javascript (via NodeJS ![h:1em](./assets/nodejs-icon-logo.svg)) 

This will be done by teaching how to print the statement "Hello World!", which is a long-standing tradition when learning a programming language 😎

---
# Vscode Docker extension

In order to be able to put code inside the container, we recommend using the [Docker extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) for vscode.

After installing the extension, you will be able to view, create and edit files inside of running containers as seen below (after creating an empty file using `touch`):

![center](./assets/vscode-docker-open-file.png)

---

# Your first Ruby ![h:1em](./assets/ruby-icon-logo.svg) program

1. Create and go to folder `class03` inside of your container's home directory (i.e. `~`)
1. Create an empty file named `hello.rb` using `touch`
1. Use **vscode** to enter this content into the the file `hello.rb`:
    ```
    puts "Hello, World!"
    ```
1. _Ensure the file has the correct contents by running `cat ruby.rb`_
1. While inside the `class03` directory, run the code using:
    ```
    ruby hello.rb
    ```
1. You should see the output "Hello, World!" printed by the program.

---

# Your first Python ![h:1em](./assets/python-icon-logo.svg) program

1. Create and go to folder `class03` inside of your container's home directory (i.e. `~`)
1. Create an empty file named `hello.py` using `touch`
1. Use **vscode** to enter this content into the the file `hello.py`:
    ```
    print("Hello, World!")
    ```
1. _Ensure the file has the correct contents by running `cat ruby.py`_
1. While inside the `class03` directory, run the code using:
    ```
    python hello.py
    ```
1. You should see the output "Hello, World!" printed by the program.

---

# Your first NodeJS ![h:1em](./assets/nodejs-icon-logo.svg) program

1. Create and go to folder `class03` inside of your container's home directory (i.e. `~`)
1. Create an empty file named `hello.js` using `touch`
1. Use **vscode** to enter this content into the the file `hello.js`:
    ```
    console.log('Hello, World!')
    ```
1. _Ensure the file has the correct contents by running `cat ruby.js`_
1. While inside the `class03` directory, run the code using:
    ```
    node hello.js
    ```
1. You should see the output "Hello, World!" printed by the program.

---

# How does the output looks like?

It looks something like this (with the **FiraCode NF Retina** nerd font):

![center](./assets/hello-world-output.png)

---

# What if this is just too much for me?

***Do not panic! We understand that not everyone will be able to do this perfectly***

Do as much as you can. In the next class, we will have an assessment of the pace of the course to keep everyone happy and engaged 😃

---

# What if it is too little for me?

Some more readings and tutorials for you:
1. [Incredibly clear and a great, 3 hour tutorial on YouTube](https://www.youtube.com/watch?v=2PGnYjbYuUo)
1. [Linux shell tutorial](https://docs.microsoft.com/en-us/learn/paths/shell/), suggested by Zhuopeng
1. [Another Linux shell tutorial](https://linuxjourney.com/lesson/the-shell), suggested by Zhuopeng

---

# What about PowerShell

For the most part, PowerShell tries to work similarly to Bash, because it was preceded by it, however, it is really quite different and is based on completely different principles:
- PowerShell and Windows do not distinguish between small- and big-cap letters, whereas bash and Linux do. In bash, all commands and options are in smallcaps.
- PowerShell uses very elaborate names for its commands, like `New-Item`, `Move-Item` and `Copy-Item` instead of `touch`, `mv` and `cp` in Bash.
- Despite the elaborate naming in PowerShell, Bash command names are offered as aliases to ease the transition. Try `Get-Alias ls`, for example.
- There is a `man` command in PowerShell, which is an alias for `Get-Help`
---

## Checklist for next class

1. _Experiment with the bash shell inside the container_, as much as you can. Exit the container cleanly using `exit` at the end of every session, this includes the full content of this presentation, including the class activity 8-)
1. _Create and run_ a simple script in Python ![h:1em](./assets/python-icon-logo.svg), Ruby ![h:1em](./assets/ruby-icon-logo.svg) and NodeJS ![h:1em](./assets/nodejs-icon-logo.svg). If you need help, _reach out to your classmates on the appropriate #discord-channel_
1. **Instructions for submission will be provided before the deadline, which is on 26.04 at midnight.**
