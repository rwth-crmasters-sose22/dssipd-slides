---
marp: true
---

# DSSIPD-SoSe22 - Class 03
## The Shell

---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
</style>

# 操作系统，种子类比  

![h:500px center](./assets/seed-analogy.drawio.png)

<!--

This is a confusing comment

-->
---

# Bash和PowerShell

在这门课中，我们试着对这两个shell达到一个舒适的水平。
- Bash
- PowerShell

一个创新者很可能不得不经常与Bash shell互动，但有时也会在Windows上使用PowerShell。

因此，必须指出的是，对Bash的适应比PowerShell更重要。然而，一个人不能没有能力在PowerShell中找到自己的方式，因为这将损害他们的创新能力。

---

# 学习策略

> 与一般人的想法不同，平行学习类似的技术可以强化共同的概念，同时在较早的阶段就能更清楚地强调差异。

> 这将使学习者在被迫在两种类似的技术之间来回切换时不容易发生混淆。

> 一种解释是，当你平行学习时，你从一开始就为这两种类似的技术中的每一种创建了单独的心理路径，而不是意外地重复使用和改变所学的_类似知识的路径，这只会导致两者之间永远的混淆。

---

# 并行学习

> ### 用非技术性的例子解释一下

> 你现在住在德国，它与一个语言相似的邻国荷兰相邻。

> 尽管语言有相似之处，但这两个国家在文化和工作场所方面有着惊人的不同，所以要考虑到这一点（只是一个旁证）。

> 另一方面，语言是相似的。相似到足以让学习者得出许多有意义的相似之处，但也相似到如果在学习过程中没有及早确定差异和相似之处，就会出现混乱。

> 换句话说，我们希望同时享受说德语和荷兰语的乐趣，而不是一直在这两种语言之间痛苦地切换。

---

# 在我们开始之前

在Discord上现场试用**Coffe bot** @Coffee  

> 这个机器人一个月的费用是10欧元。我们希望做出决定，是否愿意资助它3个月，以帮助我们进行社交。

- 通过局域网为后面的人分享屏幕。[https://192.168.0.178:8443/](https://192.168.0.178:8443/)
- 请使用会议名称 "test" 来进行上述的屏幕共享。

---

# docker命令

在所有的电脑上，我们必须具备以下条件。
- 一个_Windows Terminal_（在我们的笔记本电脑上：**Windows Terminal**，通过搜索`wt`找到）。
- 一个_Nerd字体_ (**FiraCode NF Retina**推荐)
- 设置你的控制台以使用Nerd字体。
- `docker`命令

使用`docker`命令，我们能够从命令行中启动不同的预先定制的外壳、环境和应用程序。

---

# 该类的docker容器

可以使用docker命令运行的预先定制的shell、环境和应用程序都是从[Docker Hub](https://hub.docker.com)网站上提取的。

Docker<mark>Hub</mark>中的"<mark>Hub</mark>"一词，类似于Git<mark>Hub</mark>中的 "Hub "部分。

这些都是为软件组件服务的注册机构。在GitHub和GitLab的情况下，我们称这些仓库。在Docker hub的情况下，我们称这些为容器。

我们今天上课要用的容器是`orwa84/shell-class`容器，它被托管在Docker Hub上，地址是[https://hub.docker.com/r/orwa84/shell-class](https://hub.docker.com/r/orwa84/shell-class)。

---

# 指挥基本知识

```bash
command subcommand -a -b -c --abc --cde target
```

**解释:**
- <b>command</b>: 这是主命令（例如 "docker"、"git"、"apt"、"npm"，等等）。
- <b>subcommand</b>: 这是<b>命令</b>提供的许多功能之一（例子包括 "运行"、"克隆"、"更新"、"安装 "等等）。
- <b>a</b>, <b>b</b> and <b>c</b>: 是一个字符的选项或修饰语，用于告诉<b>子命令</b>做什么，或如何做。
- <b>abc</b> and <b>cde</b>: 是多个字符的选项，或修饰语，用于告诉<b>子命令</b>做什么，或如何做。
- <b>target</b>: 是被操作的对象，如文件名、引用、URL等。

---

# Docker命令为该类运行容器

打开你的Windows Terminal，输入以下内容。

```bash
docker run -i -t --rm orwa84/shell-class
```

**解释:**
- <b>docker</b>: 这是主命令
- <b>run</b>: 这是子命令，它[运行一个新的容器](https://tldr.ostera.io/docker-run)
- <b>-i</b> and <b>-t</b>: 这些选项一起使用（也可以简写为<b>-it</b>）。它们要求Docker运行容器化的应用程序，并让你对它进行交互式访问，而不是在后台运行它。
- <b>--rm</b>: 告诉docker在容器退出后将其删除（用于测试目的）。
- <b>orwa84/shell-class</b>: 是[Docker Hub](https://hub.docker.com/r/orwa84/shell-class)上的容器参考。


---

## 上一个docker命令的问题

切换到**Docker桌面窗口**，查看你刚刚创建的运行中的容器。
注意，运行中的容器将被赋予一个随机的名字（类似于 "nervous_heyrovsky", "affectionate_haibt", 和其他奇怪但幽默的标题）。

_在交互式提示符下，键入命令<b>exit</b>，看着创建的容器消失。

这是由于在上一张幻灯片中使用了<b>--rm</b>选项。

---

# 替代的Docker命令

现在，打开你的Windows Terminal，输入以下内容。

```bash
docker run -i -t --name=class03 orwa84/shell-class
```

**补充说明:**
- <b>--name</b>: 这是一个选项，可以给运行中的容器一个你选择的名字。
- <b>=class03</b>: <b>--name</b>选项需要额外的信息，也就是你想在运行容器后给它的名字。 这被称为选项密钥对，可以被指定为 <b>option=value</b> 或 <b>option value</b> (使用一个空白)。


---

# 容器内

一旦上述命令被执行. 你_就不再在你的本地环境中输入命令了_。 相反，你不是在一个容器化的Linux环境中工作。

我为这门课制作的容器是基于Ubuntu Linux的，正在运行Bash shell，并安装了以下编程语言。
- Ruby (使用 <b>ruby</b>, <b>irb</b> 和 <b>erb</b> 指令).
- Python (使用 <b>python</b>, <b>pip</b> 和  指令)
- Javascript (使用 <b>node</b>, <b>npm</b> 和 <b>yarn</b> 指令)

我们将在本课程中讨论所有这些命令。所以不要担心。

---
# 班级活动
## 获取所有命令的版本

现代命令也被称为 _tooling_。 

_Tooling_ 通常都支持一个多字符的选项来调用：<b>version</b>。

使用第9张幻灯片中解释的现代命令的一般结构，尝试用<b>version</b>选项调用前一张幻灯片中的每一条命令，以检测运行中的容器内提供的所有工具的版本。

---

# Shell scopes

To exit the container or any other scope, we have a choice between:
要退出容器或任何其他环境，可以通过:
- 输入<b>exit</b>
- 按下<kbd>CTRL</kbd> + <kbd>C</kbd>
<!-- - Typing the command <b>exit</b> and pressing <kbd>ENTER</kbd>
- Using the keyboard shortcut <kbd>CTRL</kbd> + <kbd>C</kbd> -->

<!-- This applies to both exiting from the container, as well as existing from any nested scope _inside_ the container (e.g. if you use the <b>irb</b> command to launch the interactive Ruby prompt inside the container) -->
这既适用于从容器中退出，也适用于从容器内部的任何嵌套作用域中存在（例如，如果你使用 <b>irb</b> 命令在容器内部启动交互式 Ruby 提示）。

---

<!-- # Shell scope illustration -->
# Shell 作用域

<!-- > **[Local PowerShell prompt here]**
> _docker command to run or start **orwa84/shell-class**_
> > **[Bash shell inside the container]**
> > _<b>irb</b> command to launch the interactive Ruby_
> > > **[Ruby shell inside the bash shell inside the container]**
> > > <kbd>CTRL</kbd> + <kbd>D</kbd>
> >
> > Back to the bash shell inside the container
> > _type <b>exit</b> and press <kbd>ENTER</kbd>_
> >
> Back to the local PowerShell prompt 
> _type <b>exit</b> and press <kbd>ENTER</kbd>_


**The PowerShell windows closes...** -->
> **[本地 PowerShell]**
> _使用docker命令启动 **orwa84/shell-class**_
> > **[Docker容器中的Bash shell]**
> > _使用<b>irb</b>命令启动交互式Ruby程序_
> > > **[Docker容器中的Bash shell中的Ruby shell]**
> > > <kbd>CTRL</kbd> + <kbd>D</kbd>
> >
> > 返回到Docker容器中的Bash shell
> > _输入 <b>exit</b> 并按 <kbd>ENTER</kbd>_
> >
> 返回到本地 PowerShell 
> _输入 <b>exit</b> 并按 <kbd>ENTER</kbd>_


**Powershell命令行关闭**

---
<!-- # Restarting the container, error

So you exited the container and now want to start it again, but you encounter the following error message when using the command in **slide 12**:
```
docker: Error response from daemon: Conflict. The container name "class-03" is already in use by container.
You have to remove (or rename) that container to be able to reuse that name. 
``` -->
# 重新启动容器，错误

现在你退出了容器，想再次启动它，但在使用**第12张幻灯片**中的命令时，你遇到了以下错误信息。
```
docker: Error response from daemon: Conflict. The container name "class-03" is already in use by container.
You have to remove (or rename) that container to be able to reuse that name. 
```


<!-- **Reason for the error**

The <b>docker</b> main command has many subcommands, as mentioned.

The <b>run</b> subcommand, only [starts new containers](https://tldr.ostera.io/docker-run).

To restart an existing container, one must use the <b>start</b> subcommand, which [starts an existing, but an exited/stopped container](https://tldr.ostera.io/docker-start). -->

**出错的原因**

<b>docker</b>主命令有许多子命令，如前所述。

<b>run</b>子命令，只能用于[启动新的Docker容器](https://tldr.ostera.io/docker-run)。

要重新启动一个现有的容器，必须使用<b>start</b>子命令，它[启动一个现有的、但已退出/停止的容器](https://tldr.ostera.io/docker-start)。

---

<!-- # Restarting the container, the right command

```bash
docker start -i -a class-03
``` -->

# 重启Docker容器，正确的命令是

```bash
docker start -i -a class03
```

<!-- **Explanation:**
- <b>docker</b>: this is the main command
- <b>start</b>: this is the subcommand, it [starts a stopped or an exited container](https://tldr.ostera.io/docker-start)
- <b>-i</b> and <b>-a</b>: these options are used together (can also be abbreviated as <b>-ia</b>). They ask Docker to run the containerized application and give you interactive access to it rather than run it in the background.
- <b>class-03</b>: is the name given to the stopped or the exited container (from the <b>docker run</b> command in **slide 12**). -->

**解释：**
- <b>docker</b>：这是主要命令
- <b>start</b>：这是子命令，它用于[启动一个停止或退出的容器](https://tldr.ostera.io/docker-start)
- <b>-i</b>和<b>-a</b>：这些选项应一起使用（也可以缩写为<b>-ia</b>）以在Docker启动Docker容器的同时让您以交互模式而不是在后台运行它。
- <b>class-03</b>：是需停止或退出容器的名称（来自 **12号幻灯片** 中的 <b>docker run</b> 命令）。

---

<!-- # Grading method

> Inside the container, everything you type is recorded to assist in the grading process.

The grading process is based on the length of experimentation attempted by you inside the container. In fact you will be graded by a computer script that compares your experimentation to everyone else in the class (called [relative grading](https://www.teachmint.com/glossary/r/relative-grading/)).

The experimentation follows the _self-learning method_, where you have the ability to inquire about commands, then use the information (along with your observations) to gain insight into how these commands (and the shell overall, works).

This insight will then heavily utilized throughout the DSSIPD and DDP modules, which will be sufficient to _internalize the knowledge_. -->
# 评分办法

> 在容器内，您输入的所有内容都会被记录下来，作为评分依据。

评分办法基于您在Docker容器内尝试的实验长度。 事实上，你将被一个脚本评分，该脚本会将你的探索过程与班上其他人进行比较（称为 [relative grading](https://www.teachmint.com/glossary/r/relative-grading/)）。

实验遵循_自学方法_，您可以在其中查询命令，然后使用信息（连同您的观察结果）深入了解这些命令（以及整个 shell 的工作原理）。

然后，这种模式将在整个 DSSIPD 和 DDP 课程中大量使用，这将足以助您_内化知识_。


---

<!-- # The basics

The most useful command you need to know about is `man`.

This is the command you use to get an explanation of all the other commands. It stands for the "manual pages". In the container we use [simplified man pages](https://tldr.ostera.io/)).

Using this command, please look up and experiment with all of these commands in the container: (_these are essential commands, everyone, regardless of their background should gain comfort at using them_) -->

# 基础

您需要了解的最有用的命令是`man`。

这是您用来了解所有其他命令的命令。 它意为“manual pages”。 在Docker容器中，我们使用了 [简化版的 man ](https://tldr.ostera.io/))。

使用此命令，请在Docker容器中查找并试验所有这些命令：（_这些是必不可少的命令，每个人，无论其背景如何，都应该熟练使用_）

| `whoami` | `pwd` | `cd` | `ls` | `touch` | `mv` | `rm` | `cp` | `mkdir` | `cat` |
| -------- | ---- | ---- | ------- | ---- | ------- | ------- | ------- | ------- | ------- |

_相关谷歌搜索_：“最基本的 bash 命令”

---
<!-- # How comfortable do you need to be

The level of comfort required with the shell is the following:
1. Know where you are in the filesystem, discover what else is there, and change the working directory to where you want to go (called: _navigation_)
2. Have a good understanding of what files are hidden, and what special directories like <b>.</b>, <b>..</b> and <b>~</b> mean.
3. Create, move, copy and remove single files.
4. Create, move, and copy empty folders.
5. Move and copy folders with everything inside of them (i.e. recursively).
6. Show the contents of files in the shell. -->
# 你需要有多熟练

操作Shell所需的熟练度如下：
1. 知道你在文件系统中的位置，找出当前目录还有什么文件，把工作目录改到你想去的地方（叫做：_navigation_）
2. 了解隐藏了哪些文件，以及<b>.</b>、<b>..</b>、<b>~</b>等特殊目录的含义。
3. 创建、移动、复制和删除单个文件。
4. 创建、移动和复制空文件夹。
5. 移动和复制包含所有内容的文件夹（即递归/recursively）。
6. 在 shell 中显示文件的内容。
---

<!-- # What else?

In addition to your experimentation with the bash shell itself, we would like to teach you _how to use the 3 programming languages_ inside the shell-class container:
1. Ruby ![h:1em](./assets/ruby-icon-logo.svg)
1. Python ![h:1em](./assets/python-icon-logo.svg)
1. Javascript (via NodeJS ![h:1em](./assets/nodejs-icon-logo.svg)) 

This will be done by teaching how to print the statement "Hello World!", which is a long-standing tradition when learning a programming language 😎 -->
# 除此之外？

除了您对 bash shell 本身的实验之外，我们还想教您_如何在 shell 类容器中使用 3 种编程语言_：
1. Ruby ![h:1em](./assets/ruby-icon-logo.svg)
2. Python ![h:1em](./assets/python-icon-logo.svg)
3. Javascript（通过 NodeJS ![h:1em](./assets/nodejs-icon-logo.svg)）

这将通过教授如何输出语句“Hello World!”来完成 —— 这是我们学习编程语言时的悠久传统😎

---
<!-- # Vscode Docker extension

In order to be able to put code inside the container, we recommend using the [Docker extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) for vscode.

After installing the extension, you will be able to view, create and edit files inside of running containers as seen below (after creating an empty file using `touch`):

![center](./assets/vscode-docker-open-file.png) -->
# Vscode Docker 插件

为了能够将代码放入容器中，我们建议 vscode 安装 [Docker 插件](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)。

安装插件后，您将能够查看、创建和编辑正在运行的容器内的文件，如下所示（使用 `touch` 创建空文件后）：

![center](./assets/vscode-docker-open-file.png)
---

<!-- # Your first Ruby ![h:1em](./assets/ruby-icon-logo.svg) program

1. Create and go to folder <b>class03</b> inside of your container's home directory (i.e. <b>~</b>)
1. Create an empty file named <b>hello.rb</b> using <b>touch</b>
1. Use **vscode** to enter this content into the the file <b>hello.rb</b>:
    ```
    puts "Hello, World!"
    ```
1. _Ensure the file has the correct contents by running <b>cat ruby.rb</b>_
1. While inside the <b>class03</b> directory, run the code using:
    ```
    ruby hello.rb
    ```
1. You should see the output "Hello, World!" printed by the program. -->

# 你的第一个 Ruby ![h:1em](./assets/ruby-icon-logo.svg) 程序

1. 创建并转到容器主目录内的文件夹 <b>class03</b>（即 <b>~</b>）
2. 使用 <b>touch</b> 创建一个名为 <b>hello.rb</b> 的空文件
3. 使用 **vscode** 将此内容输入到文件 <b>hello.rb</b> 中：
     ```
     puts "Hello, World!"
     ```
4. _运行<b>cat ruby.rb</b>并确保文件内容正确_
5. 在 <b>class03</b> 目录中，使用以下命令运行代码：
     ```
     ruby hello.rb
     ```
6. 程序输出“Hello, World!” 。

---

<!-- # Your first Python ![h:1em](./assets/python-icon-logo.svg) program

1. Create and go to folder <b>class03</b> inside of your container's home directory (i.e. <b>~</b>)
1. Create an empty file named <b>hello.py</b> using <b>touch</b>
1. Use **vscode** to enter this content into the the file `hello.py`:
    ```
    print("Hello, World!")
    ```
1. _Ensure the file has the correct contents by running <b>cat ruby.py</b>_
1. While inside the <b>class03</b> directory, run the code using:
    ```
    python hello.py
    ```
1. You should see the output "Hello, World!" printed by the program. -->
# 你的第一个 Python ![h:1em](./assets/python-icon-logo.svg) 程序

1. 创建并转到容器主目录内的文件夹 <b>class03</b>（即 <b>~</b>）
2. 使用 <b>touch</b> 创建一个名为 <b>hello.py</b> 的空文件
3. 使用 **vscode** 将此内容输入到文件 `hello.py` 中：
     ```
     print("Hello, World!")
     ```
4. _运行<b>cat ruby.py</b>并确保文件内容正确_
5. 在 <b>class03</b> 目录中，使用以下命令运行代码：
     ```
     python hello.py
     ```
6. 程序输出“Hello, World!”。
---

<!-- # Your first NodeJS ![h:1em](./assets/nodejs-icon-logo.svg) program

1. Create and go to folder <b>class03</b> inside of your container's home directory (i.e. <b>~</b>)
1. Create an empty file named <b>hello.js</b> using <b>touch</b>
1. Use **vscode** to enter this content into the the file <b>hello.js</b>:
    ```
    console.log('Hello, World!')
    ```
1. _Ensure the file has the correct contents by running <b>cat ruby.js</b>_
1. While inside the <b>class03</b> directory, run the code using:
    ```
    node hello.js
    ```
1. You should see the output "Hello, World!" printed by the program. -->
# 你的第一个 NodeJS ![h:1em](./assets/nodejs-icon-logo.svg) 程序

1. 创建并转到容器主目录内的文件夹 <b>class03</b>（即 <b>~</b>）
2. 使用 <b>touch</b> 创建一个名为 <b>hello.js</b> 的空文件
3. 使用 **vscode** 将此内容输入到文件 <b>hello.js</b> 中：
     ```
    console.log('Hello, World!')
     ```
4. _运行<b>cat ruby.js</b>并确保文件内容正确_
5. 在 <b>class03</b> 目录中，使用以下命令运行代码：
     ```
     node hello.js
     ```
6. 程序输出“Hello, World!”。
---

<!-- # How does the output looks like?

It looks something like this (with the **FiraCode NF Retina** nerd font):

![center](./assets/hello-world-output.png) -->
# 输出结果应为如何？

它看起来应该像这样（使用 **FiraCode NF Retina** Nerd 字体）：

![center](./assets/hello-world-output.png)

---

<!-- # What if this is just too much for me?

***Do not panic! We understand that not everyone will be able to do this perfectly***

Do as much as you can. In the next class, we will have an assessment of the pace of the course to keep everyone happy and engaged 😃 -->
# 如果这对我来说太难了怎么办？

***不要恐慌！ 我们知道不是每个人都能完美地做到***

能做多少做多少。下节课，我们会对课程的进度进行评估，让每个人都开心和投入😃
---

<!-- # What if it is too little for me?

Some more readings and tutorials for you:
1. [Incredibly clear and a great, 3 hour tutorial on YouTube](https://www.youtube.com/watch?v=2PGnYjbYuUo)
2. [Linux shell tutorial](https://docs.microsoft.com/en-us/learn/paths/shell/), suggested by Zhuopeng
1. [Another Linux shell tutorial](https://linuxjourney.com/lesson/the-shell), suggested by Zhuopeng -->
# 如果对我来说太容易了怎么办？

为您提供更多阅读材料和教程：
1. [YouTube 上非常清晰和精彩的 3 小时教程](https://www.youtube.com/watch?v=2PGnYjbYuUo)
2. [Linux shell 教程](https://docs.microsoft.com/en-us/learn/paths/shell/)，卓鹏推荐
3. [另一个Linux shell教程](https://linuxjourney.com/lesson/the-shell)，卓鹏推荐

---

## Checklist for next class

1. _Experiment with the bash shell inside the container_, as much as you can. Exit the container cleanly using `exit` at the end of every session, this includes the full content of this presentation, including the class activity
1. _Create and run_ a simple script in Python ![h:1em](./assets/python-icon-logo.svg), Ruby ![h:1em](./assets/ruby-icon-logo.svg) and NodeJS ![h:1em](./assets/nodejs-icon-logo.svg). If you need help, _reach out to your classmates on the appropriate #discord-channel_
1. **Instructions for submission will be provided before the deadline, which is on 26.04 at midnight.**

## 下一课前的需做清单

1. _尽可能多地在容器内试验 bash shell_。 在每个会话结束时使用 `exit` 干净地退出容器，这包括此演示文稿的全部内容，包括课堂活动
2. _创建并运行_ 一个简单的 Python 脚本！[h:1em](./assets/python-icon-logo.svg), Ruby ![h:1em](./assets/ruby-icon-logo.svg) 和 NodeJS ![h:1em](./assets/nodejs-icon-logo.svg)。 如果您需要帮助，_在适当的 #discord-channel_ 上与您的同学联系
3. **提交说明将在截止日期前提供，截止日期为 26.04 午夜。**