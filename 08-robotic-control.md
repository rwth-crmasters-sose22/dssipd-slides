---
marp: true
---

# DSSIPD-SoSe22 - Class 08
## Robotic Control using Node-RED & MQTT

---

<style>
img[alt~="center"], .center {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.clear-none {
    width: 500px;
    float: left;
}
li {
    clear: none ;
}
.padding-top {
    margin-top: 1em;
}
.padding-top2 {
    padding-top: 1em;
}
.fence {
    float: right;
    align-content: center;
}
</style>

# Before we begin

- Everyone will get badges to be able to participate in the 3 days from Monday 20.06 (_Open Campus Week_, invitation and agenda will be sent via email) 
- Discuss the Rubrik for the GitLab-Markdown assignment and the goal of this assignment
- Pre-evaluation assessment: https://www.menti.com/ub52ubp4y9
- Next week are the formal evaluations, please do not miss next week's sessions!

---

# Software integration (refresher)

> Integrators use _glue logic_ to bring together a variety of software components in the form of: repos, docker containers, public API's, and others.

::: fence

@startuml

[Integrator Glue Logic] as glue #Orange
() shell
[Python repo from GitHub\nrunning on a your laptop] as python
[NodeJS repo from GitLab\nrunning on a Raspberry Pi] as nodejs
() MQTT
[Public REST API] as api
() HTTPS
() HTTP
[Containerized Docker application\nrunning on your computer] as docker

shell .. python
MQTT .. nodejs
HTTPS .. api
HTTP .. docker
glue -> shell
glue -> MQTT
HTTPS <- glue 
HTTP <- glue

@enduml

:::

---

# This week's project

::: fence

@startuml

[Integrator Glue Logic] as glue #Orange
[NodeJS repo from GitLab\nrunning in the browser] as nodejs
() MQTT
() [Node-RED ui nodes]
[Node-RED Dashboard providing\na web interface to control the excavator] as dashboard

MQTT .. nodejs
[Node-RED ui nodes] .. dashboard
glue -> [Node-RED ui nodes]
glue -> MQTT

@enduml

:::

---

# Agenda

- Introduce the _Projects_ feature in NodeRED
- Introduce the _browser-based excavator simulator_ in the **course-material** subgroup
- Control the excavator model using MQTT (the fun part)

---

# Node-RED Settings

NodeRED can be configured in a few ways. The settings are usually in a file at this location on your Laptop/PC:

```
~/.node-red/settings.js
```

For a list of some of the options and what they mean, checkout [this link](https://nodered.org/docs/user-guide/runtime/configuration).

For our purposes here, we are mostly interested in one feature, called **Projects**.

---

# Preferred way of Activating Projects

To do this right, one must edit the Node-RED settings file:

```
~/.node-red/settings.js
```

... and edit the corresponding section to activate this feature. The javascript file, contains structured information.

The path for the exact setting is `editorTheme.projects.enabled`. Please change this setting from `false` to `true` and run NodeRED, again.

---

# Node-RED command-line output changes

After running the Node-RED command, you will see the following information:

- The settings file location on your Laptop/PC
- If the projects feature is:
    - _false or disabled_, then you will see the location of the flows file.
    - _true or enabled_, then you will see the projects directory

---

# Node-RED interface changes

After running the Node-RED command, with the projects enabled. You will see the projects item as the first item in the _hamburger-icon_ ![h=1em](https://upload.wikimedia.org/wikipedia/commons/b/b2/Hamburger_icon.svg) menu.

![](./assets/nodered-projects-feature.png){.center}

---

# Another way to activate the Projects feature

Using the command-line invocation of Node-RED, it is indeed possible to override only the `editorTheme.projects.enabled` setting in Node-RED.

This is a class activity, as we work through the logical thought process to achieve this.

---

# Answer to the activity (1)

Using the `--help` option with the `node-red` command yields the following:

```
Node-RED v2.1.4
Usage: node-red [-v] [-?] [--settings settings.js] [--userDir DIR]
                [--port PORT] [--title TITLE] [--safe] [flows.json]
       node-red admin <command> [args] [-?] [--userDir DIR] [--json]

Options:
  -p, --port     PORT  port to listen on
  -s, --settings FILE  use specified settings file
      --title    TITLE process window title
  -u, --userDir  DIR   use specified user directory
  -v, --verbose        enable verbose output
      --safe           enable safe mode
  -D, --define   X=Y   overwrite value in settings file
  -?, --help           show this help
  admin <command>      run an admin command

Documentation can be found at http://nodered.org
```

---

# Answer to the activity (2)

Using the previous information, it is clear that the `--define` option (`-D` for short) is the one that needs to be used.

To make up the right pair of "setting=value" (i.e. `X=Y`), we need to take into account the setting path and the value (i.e. `true` vs. `false`) desired. This yields the following command:

```
node-red --define editorTheme.projects.enabled=true
```

After the class, please activate the Projects feature in the right way by modifying the `settings.js` file.

---

# Answer to the activity (3)

Can we do the above when running Node-RED with docker? The answer is yes!

In the `docker run` page, we are told that everything that follows the `target` (i.e. the docker image identifier) is passed to the internal command in the container.

Hence, we just need to add the `-D` part afterwards, as follows:

```
docker run --rm -p 1880:1880 nodered/node-red -D editorTheme.projects.enabled=true
```

Note that this is just for trial/evaluation purposes and the data will be wiped after you close the container.

---

# Class Checklist

- [x] Install NodeRED on your device using the `nvm` and `npm` (i.e. a local installation rather than just running via Docker)
- [ ] Change the `settings.js` file to activate the Projects feature
- [ ] Setup SSH certificate on your device
- [x] Create a personal repo in [this subgroup](https://gitlab.com/rwth-crmasters-sose22/course-work/excavator-control) on GitLab with the following options:
    - Privacy to _public_
    - Do **NOT** initialize with a README.md file
- [x] Create a new Project in Node-RED
- [x] Create an initial commit into the project
- [x] Push to your repo
---

# Missing steps

Please revisit the _incomplete_ lecture linked on Moodle. Using the information in th recording and previous recordings/slides please do the following:

- [ ] Change the `settings.js` file to activate the Projects feature
- [ ] Setup SSH certificate on your device (if you have not already done that)

If you are facing difficult problems like the ones encountered by _Davide_ or _Rodrigo_ please reach out on Discord so that we continue troubleshooting the issues you are facing.

---

# To-Do Until 14.06 Midnight

- Complete checklist
- Submit [this assignment](https://moodle.rwth-aachen.de/mod/assign/view.php?id=990242)
- Watch the supplemental videos on Moodle
- Clone and check my [NodeRED excavator control dashboard](https://moodle.rwth-aachen.de/mod/url/view.php?id=990261) made in class
- Build your own dashboard in NodeRED and test it over the next week
- Push the changes to GitLab before deadline
- Submit a screenshot of your dashboard to [this assignment](https://moodle.rwth-aachen.de/mod/assign/view.php?id=990249)
