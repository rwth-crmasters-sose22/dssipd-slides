---
marp: true
---

# DSSIPD-SoSe22 - Class 02
## Markdown

---

# Before we Begin

- Amazing turnaround! Only 2 people still did not submit the last assignment.
- Thank you for being amazing students!

---

# Agenda

- Self-study
- Create a markdown file in the [markdown-gitlab](https://gitlab.com/rwth-crmasters-sose22/course-work/gitlab-markdown) GitLab repository following the _Contribution Guidelines_ of that repository.
- Edit the file using the GitLab's Web IDE
- Use the preview tab to view the _rendered file_
- Commit to the **main** when satisfied

---

## Self-study


_In DSSIPD, the teacher is merely a facilitator of your self-learning._

Self-learning is best achieved via rapid cycles of reading and applying the knowledge using the right channels.

Only through hands-on experimentation, can the knowledge acquired be internalized.

---

## Experimentation channel

In today's class, we demonstrate the way to self learn using Gitlab Markdown Editor (i.e. Web IDE) as the experimentation channel, using only your GitLab account, and a web browser!

![](./assets/gitlab-web-ide.png)

---

## Self-learning resources

- Videos (check the `Upload` folder on `cr-storage` in the classroom's network)
- Beginner: [Online Markdown tutorial](https://www.markdowntutorial.com/) - _for the basics_
- [Kramdown](https://kramdown.gettalong.org/quickref.html) - _some of this might not be supported on GitLab_
- [GitLab-flavour Markdown](https://docs.gitlab.com/ee/user/markdown.html) - _these are all supported on GitLab_
- [PlantUML diagrams](https://plantuml.com/sitemap-language-specification) - _this diagram language is supported on GitLab inside of a **plantuml** codeblock_
- [Mermaid diagrams](https://mermaid.live) - _this diagram language is supported on GitLab inside of a **mermaid** codeblock_

---

## Differentiated by design

This is not a classical course where everyone learns the same things.

Hence you are not expected to go through the learning resources in a linear order. Rather, to pick and choose what you suspect to be of use to you, in your DDP project (e.g. how to make a _mindmap_). 

Learning is achieved via experimentation, which initiates the process of _internalizing the technical knowledge_.

---

## Internalizing the knowledge
> ### NON-TECHNICAL EXAMPLE OF WHAT WE MEAN BY THAT

> Everybody knows that people like a compliment.
>
> But if you are not the kind of a person who gives compliments, it is impossible to change your  behavior, just by knowing this simple fact.
>
> Internalizing the knowledge, is when you find  out, through experience, that people react better to you when you compliment them, so you start, actively, complimenting people, or channeling your desire for a better reaction, through giving compliments.
>
> Only through change of behavior, is the fact above truly learnt, or internalized.

---

## Internalizing the knowledge

But how does this apply to learning Markdown, or any other technical skill, for that matter?

When you copy and paste Markdown examples, you are not really learning. The content is copied as is, without the need to comprehend how special characters, like `*`, `_`, `[`, `]`, `` ` ``, had influenced its rendering.

Instead, you should read about the special rendering character, what it does, and try to experiment with it in direct, indirect, and completely novel ways.

---
## Markdown using GitLab

Please visit the [course-work](https://gitlab.com/rwth-crmasters-sose22/course-work) subgroup on GitLab.

From there, navigate to the [gitlab-markdown](https://gitlab.com/rwth-crmasters-sose22/course-work/gitlab-markdown) repository.

Follow the instructions shown in the `README.md` file.

---

## Checklist for next class

- Please continue to experiment with Markdown, using your personal file in the repository.
- Consider teaching yourself how to do the following:
  - Using codeblocks with highlighting of different programming languages
  - Using codeblocks with PlantUML content, to create a _Mindmap_ inside of a GitLab Markdown file
  - Create checklists
  - Use [Emojis](https://getemoji.com/)
  - Have fun doing all the above ✨