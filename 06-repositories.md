---
marp: true
---

# DSSIPD-SoSe22 - Class 06
## Repositories

---

<style>
img[alt~="center"], .center {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.clear-none {
    width: 500px;
    float: left;
}
li {
    clear: none ;
}
.padding-top {
    margin-top: 1em;
}
.padding-top2 {
    padding-top: 1em;
}
</style>

# Before we begin

- DDP class to start at 14:00 instead of 13:30 (consensus needed)
- Yesterday showed me that there is a misunderstanding of the _mind mapping technique_ which is intended to systematically explore a _problem space_ ([slides](https://rwth-crmasters-sose22.gitlab.io/ddp-slides/01-concept-finding.en.html#9)).
- Coffee bot in Discord is active! Is it working for you?
- **Requirements of next week's DDP presentations:**
    - collected _real-life stories from construction_
    * a mindmap tracing these stories backs to a _root idea_
    * ideas at every level of the mindmap are _mutually exclusive_
    * a branch concept of interest backed up with open-source repositories that make achieving that branch concept possible

---

# git-class assignment remarks (1)

## Chaotic commit history
![h:350px](./assets/git-workflow-1.png){.float-right} Did you merge correctly? Did you fetch before you merge? Did you merge from the master while being on the correct working branch? 
```
git checkout <my-branch>
git fetch
git merge origin/master
```

---

# git-class assignment remarks (2)

# Overwriting others work

This happened a lot, because many students did not do any research on merge conflicts and how to resolve them using vscode.

In order to help you acquire this knowledge, we have uploaded a class activity [here](https://gitlab.com/rwth-crmasters-sose22/course-work/git-lessons/conflict)

---

# git-class assignment remarks (2)

## Conflict markers showed on website! 😲😲😲

![h:350px](./assets/git-workflow-3.png)


---

# git-class assignment remarks (3)

# Missing/Wrong screenshot

Did you run a local preview of the website on your computer?

Are you aware of the `preview_website.bat` file in the repository?

Are you a Mac OS X user, and did you download the HUGO binary to preview the website in Mac OS X?

---

# git-class assignment remarks (4)

# Error with markdown
![h:300px](./assets/git-workflow-2.png){.float-right} Mistakes like this should be caught during development using the local preview.

---

# Starship :rocket: prompt

In this class we will be using the laptops of the _CR Masters_ program to use the utilities installed and to not need to download big files during class time.

Before we do this, we would like to setup the **Starship prompt :rocket:** using the following commands:
```
choco install starship
Invoke-Expression (&starship init powershell)
```


---

# Repositories

> These are repository types which are important for innovators.

- **Shell repositories:** contain one or more shell scripts
- **Container repositories:** contain a `Dockerfile` which can be used to build and run a docker container
- **Python repositories:** contain python code and the requirements for that code
- **NodeJS repositories:** contain javascript code and the requirements and actions for that code 

---

# Tooling

Each one of the previous types of repositories features its own _tooling_.

For example, there is a docker ![h:1em](./assets/docker-icon-logo.png) tooling, python ![h:1em](./assets/python-icon-logo.svg) tooling, and NodeJS ![h:1em](./assets/nodejs-icon-logo.svg) tooling.

Tooling in modern development is one or more _main commands_ that are shipped with _numerous subcommands_.

> Remember the structure of a modern tooling command from [this](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/04-git.html#8) and [this slide](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.en.html#9)

---

# Packaged vs. Un-packaged Software

Software found on GitLab and GitHub as repositories is much different from software that we install on our computer using an executable file (called an installer).

The difference is that open-source code is mostly un-packaged software, but what does this mean?

When we write software, we usually use code written by others to achieve certain tasks, like computer vision or machine learning tasks for example.

Different tooling has different names for re-usable code, such as _packages_, _libraries_, _modules_, _gems_, etc. A good general word that we can use is **depedancies**.

---

# Tooling commands

Most modern tooling will provide:
- ==_runtime command_ or _interactive runtime command_=={.float-right} A command to run stuff
- ==_package manager_=={.float-right}A command to manage the dependencies
- ==_version manager_ or _environment manager_=={.float-right} A command to manage installations

---

# Self-study questions

> What kind of tooling command do I need to:
> - run code written in Go (a programming language)
> - check which version of Python I am using?
> - change to another version of Python
> - Install the dependencies of a NodeJS project

---

# Version manager

The most important part of an innovator's mindset is to _always use a version/environment manager_.

Hence: please **do not download Python directly using an installer**, and **do not install NodeJS directly using an installer**.

Instead:
- Use `nvm`: the nodejs version manager to install nodejs
- Use _Anaconda_: the Python environment manager to install Python.

---

# Python ![h:1em](./assets/python-icon-logo.svg) tooling

- Runtime command: _python_, _ipython_ (interactive)
- Package manager command: _pip_
- Environment manager command: _conda_ and others

Dependencies in a Python repo are usually listed in a file name `requirements.txt` which can be installed with `pip` using:
```
pip install -r requirements.txt
```

---

# NodeJS ![h:1em](./assets/nodejs-icon-logo.svg) tooling

- Runtime command: _node_
- Package manager command: _npm_ (stands for _node package manager_) or _yarn_
- Environment manager command: _nvm_ (stands for _node version manager_)

Dependencies in a Python repo are usually listed in a file name `package.json` which can be installed with `npm` using:
```
npm install
```

---

# Docker ![h:1em](./assets/docker-icon-logo.png) tooling

Docker's entire functionality is implemented using a single main command, which is `docker`.

If a repository contains a file named `Dockerfile` then it can be converted to a docker container using the command:
```
docker build .
```

---

# First Challenge

Please check the music visualizer challenge in the [course-work](https://gitlab.com/rwth-crmasters-sose22/course-work) group and the [music visualizer python repo]() in the corse-material group.

