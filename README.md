# DSSIPD SoSe 2022

## Course slides

The slides of this course are tied directly to the Markdown (i.e. `.md` files) in this repository.

## List of presentations

1. Laptop setup slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/01-laptop-setup.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/01-laptop-setup.pdf).
1. Markdown slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/02-markdown.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/02-markdown.pdf).
1. Shell slides
    - in English: [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.en.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.en.pdf).
    - in Chinese (by Zhuopeng): [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.zh.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-shell.zh.pdf).
1. Shell supplementary slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-supplementary.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/03-supplementary.pdf).
1. Git slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/04-git.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/04-git.pdf).
1. Git workflow slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/05-git-workflow.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/05-git-workflow.pdf).
1. Repositories slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/06-repositories.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/06-repositories.pdf).
1. Node-RED slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/07-nodered.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/07-nodered.pdf).
1. Robotic control slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/08-robotic-control.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/08-robotic-control.pdf).