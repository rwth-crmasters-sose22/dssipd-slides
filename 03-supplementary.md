---
marp: true
---

# DSSIPD-SoSe22 - Class 03
## The Shell - _supplementary slides_

---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
li {
    clear: right;
}
</style>


# Supplementary slides

In these slides we introduce volumes in Docker and we discuss the steps needed to submit the DSSIPD assignment.

---

# Docker commands roadmap

Our plan with regard to docker, is teaching you how to use the `docker` command to do the following basic tasks:
- [x] _==&leftarrow; done==_{.float-right}How to run a new container from Docker Hub using `docker run`
- [x] _==&leftarrow; done==_{.float-right}How to start a stopped/exited container using `docker start`
- [ ] _==&leftarrow; will be explained here==_{.float-right}How to update a stopped/exited container from Docker Hub, while keeping the data of the updated container
- [ ] _==&leftarrow; will be explained here==_{.float-right}How to run a container from Docker Hub, while accessing the data inside the container using a local folder

---

# Where is the important data stored inside containers

- Containers have their own filesystem, which is completely separate from the filesystem of the host system (i.e. your local PowerShell or Bash).
- The way we access data inside the container is using **volumes**.
- A _volume_ is a folder inside the container, that is marked as "important"

---

# Three ways to interact with volumes

1. By using the volumes section in the Docker Desktop interface:
    ![center](./assets/docker-desktop-volumes.png)
1. By using the `docker run` command, you can migrate volumes from one container to another using the option `volumes-from`
1. By mapping the volume inside the container to a directory in your local system using the option `volume` (multi-character) or the shortcut `v` (single character).

---

# Method 1: Download the file for the assignment

> For submitting the assignment, you will use the first and the easiest way to access the data inside the container.

Using the volumes section in the Docker Desktop interface, switch to the tab named DATA, and download the file names `history.txt` as shown below:

![center](./assets/docker-desktop-volumes-saveas.png)

---

# Submit the file on GitLab

In the [course-work](https://gitlab.com/rwth-crmasters-sose22/course-work) GitLab group, a new project had been created with the name [shell-history](https://gitlab.com/rwth-crmasters-sose22/course-work/shell-history).

Please rename the downloaded file `history.txt` to the correct name following the [Contribution Guidelines](https://gitlab.com/rwth-crmasters-sose22/course-work/shell-history/-/blob/main/README.md) of the repository.

Use the GitLab web interface (**#gitlab**) and your **@gitlab** credentials to upload the renamed file to the repo as shown below:

![center](./assets/gitlab-upload-file.png)

---

# Notes

> We will write a script that will create a vocabulary or a complete set of all the commands typed into the shell by everyone.
>
> Then, we will use [relative grading](https://www.teachmint.com/glossary/r/relative-grading/) to compare the submissions in terms of how much they score of the complete set.

Now, we proceed to learning about volumes and how to interact with them in docker.

---

# Upgrade a container while keeping data

> Assume that you have a container locally named `class03`, which was created from `orwa84/shell-class` on Docker Hub _two weeks ago_.
>
> By going to the Docker Hub website, you noticed that the `orwa84/shell-class` containerized shell had received updates _six days ago_:
> ![center](./assets/docker-hub-shellclass.png)

In two weeks, you experimented a lot inside the container, which is tracked in the **volume** inside the container. How do you upgrade the container without losing your progress (?)

---

# Method 2: Migrate data when upgrading container

``` bash
docker run -it --volumes-from=class03 --name=class03-upgraded orwa84/shell-class
```

## Explanation of new concepts
- `volumes-from=class03`: is an option for `docker run` that permits migrating the volumes from an existing container when creating a new container. In this case, we use the volumes from the `class03` container which we created two weeks ago.
- `--name=class03-upgraded`: this is the name for the newly-created container, note that we cannot use the old name since this will result in a name collision.

---

# Method 3: Use a local mount or a folder mount

``` bash
docker run -it -v $pwd:/data --name=class03-foldermount orwa84/shell-class
```

## Explanation of new concepts
- `v`: is an option that maps one folder outside the container (before the colon) to a folder inside the container (after the colon)
- `$pwd:/data` is the output of the `pwd` command substituted into before the colon. Basically, we are mapping `/data` inside the container to the `pwd` folder outside the container
- `--name=class03-foldermount`: this is the name for the newly-created container, to differentiate it from either `class03` or `class03-upgraded`.