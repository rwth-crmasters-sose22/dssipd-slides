

// (?:(?:\r?\n){2,}|(?:\r?\n)|(?:(?<=^\s*(?:[-+*>]|#+|\d+\.)\s+.+)\r?\n))

import { assert } from "console";

String.prototype.containsAny = function (patternArray) {
    assert(Array.isArray(patternArray), "containsAny: argument is not an array");

    function matchPattern(pattern) {
        assert(this.includes, "containsAny: this.includes is not defined");
        if (this.includes === undefined)
            throw new Error("containsAny: this.includes is not defined");
            
        if (pattern instanceof RegExp) {
            return pattern.test(this);
        }
        else if (typeof pattern === "string" || pattern instanceof String) {
            return this.includes(pattern);
        }
        else {
            assert.fail(`containsAny: pattern (${pattern}) is not a string or regex`);
            throw new Error("containsAny: pattern is not a string or regex");
        }
    }

    return patternArray.reduce((a, b) => {
        if (typeof a !== 'boolean')
            a = matchPattern.call(this, a);
            
        return a || matchPattern.call(this, b);
    });
};