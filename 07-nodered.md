---
marp: true
---

# DSSIPD-SoSe22 - Class 07
## Node-RED

---

<style>
img[alt~="center"], .center {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.clear-none {
    width: 500px;
    float: left;
}
li {
    clear: none ;
}
.padding-top {
    margin-top: 1em;
}
.padding-top2 {
    padding-top: 1em;
}
.fence {
    float: right;
    align-content: center;
}
</style>

# Before we begin

- Thank you for your presentations yesterday :)
- The deadline for the corrected presentations & the personal contribution documents is changed to tonight at midnight ⏰
- We hope that _you took note of everything that you had been told yesterday_ ;), and that you account for all the points in your corrected presentations (some people have more work than others)

---

# Software integration

> Integrators use _glue logic_ to bring together a variety of software components in the form of: repos, docker containers, public API's, and others.

::: fence

@startuml

[Integrator Glue Logic] as glue #Orange
() shell
[Python repo from GitHub\nrunning on a your laptop] as python
[NodeJS repo from GitLab\nrunning on a Raspberry Pi] as nodejs
() MQTT
[Public REST API] as api
() HTTPS
() HTTP
[Containerized Docker application\nrunning on your computer] as docker

shell .. python
MQTT .. nodejs
HTTPS .. api
HTTP .. docker
glue -> shell
glue -> MQTT
HTTPS <- glue 
HTTP <- glue

@enduml

:::

---

# Glue logic

Glue logic is a _term remnant from electronic design in the 80's_ where IC chips
used different voltage standards and hence needed interfacing between them to work
together.

Today, the term can be used for a program that connects multiple parts together using
broker communication (e.g. MQTT), REST API's (i.e. HTTPS), and uses some form of data
storage in the form of a containerized database server.

---

# Agenda

> Unfortunately, class time is barely enough to explore any of these topics in depth. Besides, it is hard to predict what kind of database/API will be needed by your DDP project, hence we will just demonstrate one simple possibility.

- Primer on broker communication (using MQTT)
- Primer on Node-RED
- Primer on using Node-RED to communicate with an API
- Primer on using Node-RED to communicate with a local database

---

# Direct communication (HTTP or HTTPS)

Usually, with a database server (running as a docker container) or a public API, we use direct communication using HTTP or HTTPS. This means that we use the URL to access the service (or the database) directly.

<div class="center">

::: fence

@startuml

[Integrator Glue Logic] as glue #Orange
[API or Docker container] as party

glue . party

@enduml

:::

</div>

This works fine when on the same computer or when the other party is accessible through a public URL.

---

# Brokered communication (e.g. MQTT)

However, it is often the case in robotics that the device we are communicating with is a Raspberry PI connected to a sensor/actuator, which is not hosted on a public domain or has no public URL.

Another case is when our integrated application is running on multiple laptops or PC's, which are only locally present in the network but have no public domain or has no public URL.

<div class="center">

::: fence

@startuml

[Integrator Glue Logic] as glue #Orange
() "MQTT\nbroker" as mqtt
[API or Docker container] as party
glue . mqtt
mqtt . party

@enduml 

:::

</div>

All of these cases are solved using a brokered connection, like MQTT.

---

# Brokered communication (e.g. MQTT)

Class activity at: http://www.hivemq.com/demos/websocket-client/

This web application can be found by searching for "MQTT Websocket Client" on Google.

> For more information about API's please check the video "3. Calling an API with Postman.mp4" in folder 5 in the CI/CD course in the Upload folder on the NAS.

---

# Direct communication (Public API)

Please download the [Thunder Client](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client).

Figure out how to use the [Robohash](https://robohash.org/) API.

---

# Node-RED

Node-RED is the visual programming environment we use to construct our _glue logic_

The programs look like the ones in Grasshopper (the Rhino environment), but instead of being limited to creating geometries, we can perform any kind of logic.

Since the class time is really limited, you are encouraged to use online resources and personal experimentation to get better at Node-RED. We just show the basics and the bare minimum to get you started.

---

# Installing Node-RED

There are two ways to run Node-RED on a computer:
- using `npm` to install locally on the system (Node-RED is a NodeJS application)
- using `docker` to run as a container

Node-RED can also run on Raspberry PI's and smartphones, but the options of installation on these devices are more limited.

---

# Pros and Cons of `npm` vs. `docker`


| local installation pros | local installation cons |
| ---- | ---- |
| Access to hardware/filesystem   | Only one NodeRED can exist per node version |
| Easier configuration (e.g. SSH) | Might need to switch node version using nvm |


| containerized application pros | containerized application cons |
| ---- | ---- |
| Many Node-RED instances can be launched       | Cumbersome to configure and access local data |
| Does not need `nvm`, `node` or `npm` locally  | Access to hardware might be limited  |

---

# How to install Node-RED

For full instructions on how to install Node-RED on your computer please check the following URL: https://nodered.org/docs/getting-started/

---

# Primer on Node-RED

Using our laptops please run Node-RED: 

---

# Primer on Databases using Node-RED

```
docker run -p 6379:6379 --name redis -d redis
```


